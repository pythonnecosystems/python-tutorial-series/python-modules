# Python 모듈: 임포트, 생성 및 사용 <sup>[1](#footnote_1)</sup>

> <font size="3">Python 코드가 포함된 파일인 모듈을 임포트하고 생성하고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./modules.md#intro)
1. [모듈이란?](./modules.md#sec_02)
1. [모듈 임포트 방법](./modules.md#sec_03)
1. [모듈 생성 방법](./modules.md#sec_04)
1. [모듈 사용법](./modules.md#sec_05)
1. [공통 모듈 작동](./modules.md#sec_06)
1. [요약](./modules.md#summary)


<a name="footnote_1">1</a>: [Python Tutorial 16 — Python Modules: Import, Create, and Use](https://python.plainenglish.io/python-tutorial-16-python-modules-import-create-and-use-74ff91615a0a?sk=66806bcdfef9871d3a4f8473f498338f)를 편역한 것이다.
