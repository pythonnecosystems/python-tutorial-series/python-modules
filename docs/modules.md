# Python 모듈: 임포트, 생성 및 사용

## <a name="intro"></a> 들어가며
이 포스팅에서는 Python에서 모듈(module)을 임포트하고, 만들고, 사용하는 방법을 알아 보려고 한다. 모듈은 Python 코드가 포함된 파일로 다른 Python 프로그램에서 재사용할 수 있다. 모듈은 코드를 구성하고, 반복을 방지하고, 코드를 다른 사람들과 공유하는 데 도움을 줄 수 있다.

이 포스팅의 내용을 익히면 다음 작업을 수행할 수 있다.

- Python에서 모듈이 무엇이고 어떻게 작동하는지 이해한다
- 표준 라이브러리와 외부 소스에서 모듈 임포트할 수 있다
- 자신만의 모듈을 만들어 프로그램에 사용할 수 있다
- 모듈에서 속성을 다시 로드하고, 이름을 변경하고, 액세스하는 등의 일반적인 작업을 수행할 수 있다

이 포스팅를 읽어 나가려면 Python과 데이터 분석에 대한 기본적인 이해가 필요할 것이다. 또한 Python 인터프리터와 편집기 또는 IDE가 필요할 것이다. 어떤 버전의 Python도 사용할 수 있지만 이 자습서는 Python 3.9를 기반으로 한다.

Python 모듈에 대해 배울 준비가 되었나요? 시작해 봅시다!

## <a name="sec_02"></a> 모듈이란?
모듈은 Python 코드가 포함된 파일로 다른 Python 프로그램에서 임포트하여 사용할 수 있다. 모듈은 변수, 함수, 클래스 및 dot (.) 연산자를 사용하여 액세스할 수 있는 기타 객체를 포함할 수 있다. 예를 들어, `sqrt`라는 함수가 포함된 `math`라는 모듈이 있다면 모듈을 임포트하여 함수를 `math.sqrt`로 사용할 수 있다.

모듈은 다음과 같은 몇 가지 이유로 유용하다.

- 코드를 특정 목적과 기능이 있는 별도의 파일로 구성할 수 있다.
- 모듈의 코드를 재사용하여 다른 프로그램에서 동일한 코드를 반복하여 작성하지 않도록 도와준다.
- 다른 개발자와 코드를 공유하고 표준 라이브러리 또는 타사 라이브러리와 같은 다른 소스의 코드를 사용할 수 있다.

Python에는 math, random, os, sys 등 다양한 기능을 제공하는 수많은 모듈이 내장되어 있다. 파일에 Python 코드를 작성하고 .py 확장자로 저장하여 자신만의 모듈을 만들 수도 있다. 그런 다음 다른 Python 프로그램에서 모듈을 임포트하여 그 안에 정의된 객체를 사용할 수 있다.

[다음 절](#sec_03)에서는 Python에서 모듈을 임포트하고 그 객체를 사용하는 방법을 다룰 것이다.

## <a name="sec_03"></a> 모듈 임포트 방법
Python에서 모듈을 임포트려면 `import` 문 다음에 모듈의 이름을 사용하면 된다. 예를 들어, 수학 모듈을 가져오려면 다음과 같이 작성하면 된다.

```python
import math
```

모듈을 임포트한 후 점(`.`) 연산자를 사용하여 모듈의 객체에 접근할 수 있다. 예를 들어, `math` 모듈에서 `sqrt` 함수를 사용하려면 다음과 같이 할 수 있다.

```python
import math

result = math.sqrt(25) # result is 5.0
```

여러 모듈을 쉼표로 구분하여 한 줄로 임포트할 수도 있다. 예를 들어 `random`과 `os` 모듈을 함께 임포트하여면 다음과 같이 작성하면 된다.

```python
import random, os
```

그러나, 더 나은 가독성 및 유지보수성을 위해 일반적으로 라인당 하나의 모듈을 가져오는 것을 권장한다.

때로는 전체 모듈이 아닌 모듈에서 특정 객체만 임포트하기를 원할 수도 있다. 이 경우 `from … import` 문을 사용할 수 있다. 예를 들어, `math` 모듈에서 `sqrt` 함수만 임포트하려면 다음과 같이 할 수 있다.

```python
from math import sqrt
```

`from … import` 문을 사용하여 객체를 임포트한 후에는 점(`.`) 연산자 없이 객체를 직접 사용할 수 있다. 예를 들어 `sqrt` 함수를 사용하려면 다음과 같이 쓸 수 있다.

```python
from math import sqrt
result = sqrt(25) # result is 5.0
```

모듈에서 여러 개체를 쉼표로 구분하여 임포트할 수도 있다. 예를 들어, `math` 모듈에서 `pi`와 `sin` 객체를 임포트하려면 다음과 같이 작성할 수 있다.

```python
from math import pi, sin
```

또는 별표(`*`) 기호를 사용하여 모듈에서 모든 객체를 임포트할 수 있다. 예를 들어, `math` 모듈에서 모든 객체를 임포트하려면 다음과 같이 작성할 수 있다.

```python
from math import *
```

그러나 이는 이름 충돌을 유발하고 코드의 명확성과 가독성을 저하시킬 수 있으므로 일반적으로 권장되지 않는다.

[다음 절](#sec_04)에서는 Python에서 자신만의 모듈을 만들어 프로그램에 사용하는 방법에 대해 알아본다.

## <a name="sec_04"></a> 모듈 생성 방법
Python으로 모듈을 만들려면 Python 코드를 파일로 작성하고 `.py` 확장자로 저장해야 한다. 예를 들어, 그리팅 메시지를 출력하는 `greet`이라는 함수를 포함하는 `greetings`이라는 모듈을 만들고자 한다고 가정하자. 다음 코드를 파일로 작성하여 `greetings.py` 로 저장한다.

```python
# This is the greetings module
def greet(name):
    print("Hello, " + name + "!")
```

Python으로 첫 모듈을 만들었다! 이제 다른 Python 프로그램에서 임포트하여 사용할 수 있다. 예를 들어, 다음 코드를 포함하는 `main.py` 라는 이름의 다른 파일이 있다고 가정해 보겠다,

```python
# This is the main program
import greetings # Import the greetings module


greetings.greet("World") # Call the greet function from the greetings module
```

`main.py` 파일을 실행하면 다음과 같은 출력을 얻을 수 있다.

```
Hello, World!
```

보다시피 dot(`.`) 연산자를 사용하여 `greetings` 모듈에 정의된 객체를 액세스할 수 있다. 또한 `from … import` 문을 사용하여 `greetings` 모듈에서 `greet` 함수만 임포트할 수 있다. 예를 들어 다음과 같이 작성할 수 있다.

```python
 This is the main program
from greetings import greet # Import only the greet function from the greetings module


greet("World") # Call the greet function directly
```

이 파일을 실행하면 이전과 동일한 출력이 디스플레이된다.

```python
Hello, World!
```

원하는 만큼 모듈을 만들어 프로그램에 사용할 수 있다. 그러나 모듈이 메인 프로그램과 동일한 디렉터리나 Python 경로에 있는 디렉터리에 저장되어 있는지 확인해야 한다. Python 경로는 Python에서 모듈을 검색하는 디렉터리 목록이다. `sys.path` 속성을 사용하여 Python 경로를 확인할 수 있다. 예를 들어 다음과 같이 작성할 수 있다.

```python
import sys # Import the sys module


print(sys.path) # Print the Python path
```

다음과 같은 디렉토리 목록을 얻을 수 있다.

```
['', '/usr/lib/python39.zip', '/usr/lib/python3.9', '/usr/lib/python3.9/lib-dynload', '/home/user/.local/lib/python3.9/site-packages', '/usr/local/lib/python3.9/dist-packages', '/usr/lib/python3/dist-packages']
```

Python 경로에 새 디렉터리를 추가하려면 `sys.path.append` 메서드를 사용할 수 있다. 예를 들어 커스텀 모듈이 포함된 `my_modules`라는 디렉터리가 있다면 다음과 같이 작성하여 Python 경로에 추가할 수 있다.

```python
import sys # Import the sys module


sys.path.append("/home/user/my_modules") # Add the my_modules directory to the Python path
```

Python 경로에 디렉터리를 추가한 후 그 안에 있는 모듈을 임포틀하여 사용할 수 있다.

[다음 절](./sec_05)에서는 Python에서 모듈을 임포트하고 모듈의 속성을 액세스하는 방법을 설명할 것이다.

## <a name="sec_05"></a> 모듈 사용법
Python에서 모듈을 사용하려면 앞 절에서 설명한 대로 먼저 임포트해야 한다. 모듈을 임포트한 후에는 점(`.`) 연산자를 사용하거나 모듈을 가져온 방법에 따라 직접 객체 이름을 사용하여 객체를 액세스할 수 있다. 예를 들어 import 문을 사용하여 math 모듈을 가져온 경우 점(`.`) 연산자를 사용하여 `math.pi` 또는 `math.sqrt` 같은 객체를 액세스할 수 있다. `from... import` 문을 사용하여 수학 모듈에서 `pi`와 `sqrt` 객체를 임포트한 경우 `pi` 또는 `sqrt` 같은 객체 이름을 직접 사용할 수 있다.

Python에서 모듈을 사용할 때 모듈에 대한 정보를 제공하는 몇 가지 특별한 속성에 접근할 수도 있다. 이러한 속성들은 이중 밑줄(`__`) 접두사가 붙는다. 예를 들어 다음과 같은 속성을 사용하여 `math` 모듈에 대한 정보를 얻을 수 있다.

- `__name__`: 이 속성은 모듈의 이름을 문자열로 반환한다. 예를 들어 `math.__name__`은 `'math'`를 반환한다.
- `__doc__`: 이 속성은 모듈의 문서 문자열(있는 경우)을 반환한다. 예를 들어 `math.__doc__`은 `math` 모듈과 그 함수에 대한 간략한 설명을 반환한다.
- `__file__`: 이 속성은 모듈이 정의된 파일의 경로를 반환한다. 예를 들어 `math.__file__`은 리눅스에서 `'/usr/lib/python 3.9/lib-dynload/math.cpython-39-x86_64-linux-gnu.so '`를 반환한다.
- `__dict__`: 이 속성은 모듈에 정의된 모든 객체의 이름과 값을 포함하는 사전을 반환한다. 예를 들어 `math.__dict__`은 `math` 모듈의 모든 변수, 함수, 클래스 등 기타 객체의 키와 값을 포함하는 사전을 반환한다.

이러한 속성을 사용하면 사용하는 모듈에 대한 자세한 정보를 얻을 수 있고 코드를 디버깅할 수 있다. 그러나 이러한 속성은 읽기 전용이므로 수정해서는 안 된다.

[다음 절](#sec_06)에서는 모듈에 대한 몇 가지 일반적인 작업(예: 재로드, 이름 변경, 속성 액세스)을 수행하는 방법을 다룰 것이다.

## <a name="sec_06"></a> 공통 모듈 작동
이 절에서는 모듈에 대한 몇 가지 일반적인 작업(예: 재로드, 이름 변경, 속성 액세스)을 수행하는 방법을 보일 것이다.

**모듈 재로드**: 모듈 파일을 일부 변경하여 프로그램에 적용하려는 경우 모듈을 가져온 후 다시 로드할 수 있다. 모듈을 다시 로드하려면 `importlib` 모듈에서 `reload` 함수를 사용할 수 있다. 예를 들어 `greetings` 모듈을 임포트하였고 재로드하고 싶다고 가정하면 다음과 같이 작성할 수 있다.

```python
import greetings # Import the greetings module
import importlib # Import the importlib module


importlib.reload(greetings) # Reload the greetings module
```

**모듈 이름 변경**: 모듈을 임포트한 후에 모듈 이름을 변경할 수도 있다. 예를 들어 모듈에 대해 더 짧거나 더 설명적인 이름을 사용하려면 임포트 문 다음에 `as` 키워드를 사용하면 된다. 예를 들어 `math` 모듈 이름을 `m`으로 바꾸려면 다음과 같이 사용할 수 있다.

```python
import math as m # Import the math module and rename it as m


result = m.sqrt(25) # Use the m name to access the sqrt function
```

**속성 액세스**: 예를 들어 변수에 이름이 저장된 속성을 액세스하려면 점(`.`) 연산자를 사용하지 않고 모듈의 속성을 액세스하기를 원할 수도 있다. 모듈의 속성을 액세스하려면 모듈 이름과 속성 이름을 인수로 가져와 속성의 값을 반환하는 `getattr` 함수를 사용하면 된다. 예를 들어 `math` 모듈의 `pi` 속성을 액세스하려고 하는데 속성 이름이 `attr`이라는 변수에 저장되어 있다고 가정하면 다음과 같이 사용할 수 있다.

```python
import math # Import the math module


attr = "pi" # Store the attribute name in a variable
value = getattr(math, attr) # Get the value of the attribute using the getattr function
print(value) # Print the value
```

그러면 `pi` 속성의 값인 3.141592653589793이 출력된다.

[다음인 마지막 절](#summary)에서는 포스팅을 마무리하고 추가 리소스를 제공할 것이다.

## <a name="summary"></a> 요약
Python 모듈에 대한 이 포스팅의 끝에 도달했다. 여기에서는 Python에서 모듈을 임포트하고, 생성하고, 사용하는 방법을 설명하였다. 모듈은 Python 코드가 포함된 파일로 다른 Python 프로그램에서 재사용할 수 있다. 모듈은 코드를 구성하고 반복을 방지하며 코드를 다른 사람과 공유하는 데 도움을 줄 수 있다.

다음은 학습한 내용을 요약한 것이다.

- import 문 또는 from ... import 문을 사용하여 모듈을 가져오는 방법
- 파일에 Python 코드를 작성하고 `.py` 확장자로 저장하여 모듈을 만드는 방법
- 점(`.`) 연산자 또는 객체 이름을 직접 사용하여 해당 객체를 액세스하여 모듈을 사용하는 방법
- `__name__`, `__doc__`, `__file__`, `__dict__` 등 모듈의 일부 특수 속성에 액세스하는 방법
- 모듈에서 일반적인 작업(예: 속성 재로드, 이름 변경, 속성 액세스)을 수행하는 방법

Python 모듈에 대해 더 자세히 알고 싶다면 다음 리소스를 참조할 수 있다.

- [모듈에 관한 Python 공식 문서](https://docs.python.org/3/tutorial/modules.html)
- [Real Python의 Python 모듈 및 패키지에 대한 튜토리얼](https://realpython.com/python-modules-packages/)
- [코리 쉐퍼의 Python 모듈에 관한 비디오](https://www.youtube.com/watch?v=CqvZ3vGoGs0)
